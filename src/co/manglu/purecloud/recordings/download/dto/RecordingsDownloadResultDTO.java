package co.manglu.purecloud.recordings.download.dto;

public class RecordingsDownloadResultDTO {

	private boolean success;

	private String recordingsDownloadUrl;

	public RecordingsDownloadResultDTO() {
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getRecordingsDownloadUrl() {
		return recordingsDownloadUrl;
	}

	public void setRecordingsDownloadUrl(String recordingsDownloadUrl) {
		this.recordingsDownloadUrl = recordingsDownloadUrl;
	}

}
