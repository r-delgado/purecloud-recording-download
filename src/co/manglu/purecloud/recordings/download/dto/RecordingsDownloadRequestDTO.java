package co.manglu.purecloud.recordings.download.dto;

import java.util.List;

public class RecordingsDownloadRequestDTO {

	private String jobId;

	private String expectedResultCount;

	private String resultCount;

	private String errorCount;

	private List<RecordingDTO> results;

	public String getJobId() {
		return jobId;
	}

	public void setJobId(String jobId) {
		this.jobId = jobId;
	}

	public String getExpectedResultCount() {
		return expectedResultCount;
	}

	public void setExpectedResultCount(String expectedResultCount) {
		this.expectedResultCount = expectedResultCount;
	}

	public String getResultCount() {
		return resultCount;
	}

	public void setResultCount(String resultCount) {
		this.resultCount = resultCount;
	}

	public String getErrorCount() {
		return errorCount;
	}

	public void setErrorCount(String errorCount) {
		this.errorCount = errorCount;
	}

	public List<RecordingDTO> getResults() {
		return results;
	}

	public void setResults(List<RecordingDTO> results) {
		this.results = results;
	}

}
