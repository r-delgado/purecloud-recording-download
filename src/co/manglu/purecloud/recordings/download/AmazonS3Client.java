package co.manglu.purecloud.recordings.download;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.util.IOUtils;

public class AmazonS3Client {

	private AmazonS3 s3client;

	private final static String ENDPOINT_URL = "https://s3.amazonaws.com";

	private final static String BUCKET_NAME = "gns-purextended-recordings";

	private final static String ACCESS_KEY = "AKIASNVBNXLHI5FVDV6S";

	private final static String SECRET_KEY = "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";

	public AmazonS3Client() {
		AWSCredentials credentials = new BasicAWSCredentials(ACCESS_KEY, SECRET_KEY);
		this.s3client = AmazonS3ClientBuilder.standard().withCredentials(new AWSStaticCredentialsProvider(credentials))
				.withRegion(Regions.US_EAST_1).build();
	}

	public String uploadFileTos3bucket(String fileName, InputStream memoryFile)
			throws IOException, NoSuchAlgorithmException {
		ObjectMetadata metaData = new ObjectMetadata();
		byte[] bytes = IOUtils.toByteArray(memoryFile);
		metaData.setContentLength(bytes.length);
		ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bytes);
		s3client.putObject(new PutObjectRequest(BUCKET_NAME, fileName, byteArrayInputStream, metaData)
				.withCannedAcl(CannedAccessControlList.PublicRead));
		String fileUrl = ENDPOINT_URL + "/" + BUCKET_NAME + "/" + fileName;
		return fileUrl;
	}

	public static String getFileName(String fileName, String extention, Date generationDate) {
		SimpleDateFormat format = new SimpleDateFormat("_yyyy-MM-dd_HH-mm-ss");
		return fileName + "-" + format.format(generationDate) + extention;
	}

}