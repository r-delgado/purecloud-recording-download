package co.manglu.purecloud.recordings.download;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.zip.ZipOutputStream;

import org.apache.commons.compress.archivers.zip.ZipArchiveEntry;
import org.apache.commons.io.FileUtils;

import co.manglu.purecloud.recordings.download.dto.RecordingDTO;
import co.manglu.purecloud.recordings.download.dto.RecordingsDownloadRequestDTO;

public class RecordingsDownloadService {

	public String startDownloadRecordings(RecordingsDownloadRequestDTO recordings)
			throws NoSuchAlgorithmException, IOException {
		return downloadRecordings(recordings, "recordings-download");
	}

	public String downloadRecordings(RecordingsDownloadRequestDTO recordings, String zipName)
			throws IOException, NoSuchAlgorithmException {
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		new ByteArrayInputStream(bos.toByteArray());
		ZipOutputStream zipfile = new ZipOutputStream(bos);
		byte[] buffer = new byte[1024];
		AtomicInteger index = new AtomicInteger(0);
		List<String> errors = new ArrayList<String>();
		List<RecordingDTO> errorsDto = new ArrayList<RecordingDTO>();
		recordings.getResults().forEach(recording -> {
			try {
				if (recording.getResultUrl() != null && !recording.getResultUrl().isEmpty()) {
					File tempFile = File.createTempFile("recording", ".suffix");
					FileUtils.copyURLToFile(new URL(recording.getResultUrl()), tempFile);
					String[] extention = recording.getContentType().split("/");
					String recordingName = String.format("%s_%s_%s.%s", index.incrementAndGet(),
							recording.getConversationId(), recording.getRecordingId(), extention[extention.length - 1]);
					addZipArchive(tempFile, recordingName, buffer, zipfile);
					tempFile.delete();
				} else if (recording.getErrorMsg() != null && !recording.getErrorMsg().isEmpty()) {
					errors.add(String.format("%s: %s", recording.getConversationId(), recording.getErrorMsg()));
					errorsDto.add(recording);
				} else{
					errors.add(String.format("%s: %s", recording.getConversationId(), "No error message"));
					errorsDto.add(recording);
				}

			} catch (Exception e) {
				e.printStackTrace();
				errors.add(String.format("%s: %s", recording.getConversationId(), e.getLocalizedMessage()));
				errorsDto.add(recording);
			}

		});
		recordings.getResults().removeAll(errorsDto);
		Date now = new Date();
		File report = generateReport(recordings, errors, now);
		addZipArchive(report, "report.txt", buffer, zipfile);
		report.delete();
		zipfile.close();
		String fileName = AmazonS3Client.getFileName(zipName, ".zip", now);
		AmazonS3Client s3 = new AmazonS3Client();
		return s3.uploadFileTos3bucket(fileName, new ByteArrayInputStream(bos.toByteArray()));

	}

	private void addZipArchive(File archive, String archiveName, byte[] buffer, ZipOutputStream zipFile)
			throws IOException {

		FileInputStream fis = new FileInputStream(archive);
		ZipArchiveEntry file = new ZipArchiveEntry(archive, archiveName);
		zipFile.putNextEntry(file);
		int length;
		while ((length = fis.read(buffer)) > 0) {
			zipFile.write(buffer, 0, length);
		}
		zipFile.closeEntry();
		fis.close();

	}

	private File generateReport(RecordingsDownloadRequestDTO recordings, List<String> errors, Date generationDate)
			throws IOException {

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");

		File tempFile = File.createTempFile("report", ".txt");
		FileWriter fw = new FileWriter(tempFile);
		String header = String.format(
				"Recordings Download Report:\n\nGeneration date: %s\n"
						+ "*****************************\nResults Information\n\n" + "Job Id: %s\n"
						+ "Job Url: https://api.mypurecloud.com/api/v2/recording/batchrequests/%s\n"
						+ "Expected Result Count: %s\n" + "Result Count: %s\n" + "Error Count: %s\n\n",
				sdf.format(generationDate), recordings.getJobId(), recordings.getJobId(),
				recordings.getExpectedResultCount(), recordings.getResultCount(), recordings.getErrorCount());
		fw.write(header);
		fw.write("*****************************\nFailed conversations\n\n");
		if (Integer.parseInt(recordings.getErrorCount()) > 0) {
			errors.forEach(conversationId -> {
				try {
					fw.write(String.format("%s\n", conversationId));
				} catch (IOException e) {
					e.printStackTrace();
				}
			});
		}
		fw.write("\n*****************************\nSuccess conversations\n\n");
		recordings.getResults().forEach(recording -> {
			try {
				fw.write(String.format("%s\n", recording.getConversationId()));
			} catch (IOException e) {
				e.printStackTrace();
			}
		});
		fw.flush();
		fw.close();
		return tempFile;
	}

	public static void main(String[] str) throws IOException, NoSuchAlgorithmException {

	}

}
