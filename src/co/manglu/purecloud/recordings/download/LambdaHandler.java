package co.manglu.purecloud.recordings.download;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;

import co.manglu.purecloud.recordings.download.dto.RecordingsDownloadRequestDTO;
import co.manglu.purecloud.recordings.download.dto.RecordingsDownloadResultDTO;

public class LambdaHandler implements RequestHandler<RecordingsDownloadRequestDTO, RecordingsDownloadResultDTO> {

	public String myHandler(int myCount, Context context) {
		return String.valueOf(myCount);
	}

	@Override
	public RecordingsDownloadResultDTO handleRequest(RecordingsDownloadRequestDTO recordingRequestDTO,
			Context context) {

		RecordingsDownloadResultDTO recordingsDownloadResultDTO = new RecordingsDownloadResultDTO();
		recordingsDownloadResultDTO.setSuccess(false);

		try {
			String recordingsDownloadUrl = new RecordingsDownloadService().startDownloadRecordings(recordingRequestDTO);
			recordingsDownloadResultDTO.setSuccess(true);
			recordingsDownloadResultDTO.setRecordingsDownloadUrl(recordingsDownloadUrl);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return recordingsDownloadResultDTO;
	}

}
